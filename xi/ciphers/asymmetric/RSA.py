# -*- coding: utf-8 -*-

import logging

from M2Crypto import RSA, BIO

from ..object import cipherProduct

log = logging.getLogger('Xi.ciphers.asymmetric.RSA')

class cipher:
    """Core RSA module, wrapped from OpenSSL using M2Crypto.
       
       This module shall provide basic function as encrypt, decrypt, sign,
       verify, as well as signals of can_sign, can_encrypt, etc."""
    __privateKey = None
    __publicKey = None

    def __init__(self):
        pass

    def _getSignLimit(self):
        """Calculates limit on input when signing a text."""
        return int(self.bits * 0.75 / 8)

    def generate(self,**argv):
        """Generates new key pair.

           :argv bits  Specify RSA key length."""
        if argv.has_key('bits'):
            bits = argv['bits']
        else:
            bits = 4096
        if bits < 1024:
            raise Exception("Cannot accept such bits < 1024.")
        self.bits = bits

        log.debug("Generating a %d bits RSA key, please wait." % bits)

        self.__privateKey = RSA.gen_key(bits,65537)
        
        log.debug("RSA key generation done.")

        self._derive_pubkey()
        
    def sign(self, digest):
        """Return string of signed digest. Digest is not calculated in this
           function."""
        if self.__privateKey == None:
            return False
        encrypted = self.__privateKey.private_encrypt(digest,1)
        return encrypted

    def verify(self, digest, sign):
        """Verify a sign with this key, see if it matches given digest."""
        if self.__publicKey == None:
            return False
        try:
            decrypted = self.__publicKey.public_decrypt(sign,1)
            if decrypted == digest:
                return True
        except Exception,e:
            pass
        return False

    def encrypt(self, message, encryptor):
        """Encrypt unlimited length of message. This is accomplished with an
           encryptor. The encryptor shall be a function of (key, plaintext).

             The function first generates a randomized symmetric key as long
           as possible to be encrypted using RSA, then this key is supplied to
           `encryptor` to encrypt the whole message.
           
             The function encrypts using PUBLIC key. To decrypt, you need a
           private part of this pair of key."""
        
        # Shall be used with a public key.
        if self.__publicKey == None:
            return False
        
        # Generate a temp key and encrypt it using RSA.
        tempkey = ''
        maxlen = int(self.bits * 0.5 / 8)
        for i in range(0,maxlen):
            tempkey += chr(random.randint(0,255))
        
        # encrypt the message using tempkey.
        data = encryptor(tempkey,message)
        keyinfo = self.__publicKey.public_encrypt(tempkey,4)
        
        # Write out.
        product = cipherProduct()
        product.type = 'RSA_Encrypted'
        product.tkey = keyinfo
        product.ctxt = data
        return str(product)

    def decrypt(self, ciphertext, decryptor):
        """Decrypt a bulk of ciphertext. This is a reverse function of
           self.encrypt, and a decryptor of symmetric ciphers is required.
           
             The function decrypts using the PRIVATE key."""

        # Shall be used with a private key.
        if self.__privateKey == None:
            return False

        try:
            if type(ciphertext) == str:
                j = cipherProduct(ciphertext)
            else:
                j = ciphertext
            if j.type != 'RSA_Encrypted':
                raise Exception("Input may not be the intending ciphertext.")
            tempkey = j.tkey
            ciphertext= j.ctxt
        except:
            raise Exception("Bad RSA ciphertext format.")

        try:
            tempkey = self.__privateKey.private_decrypt(tempkey,4)
        except Exception,e:
            raise Exception("Unable to decrypt this RSA ciphertext: %s" % e)
        return decryptor(tempkey,ciphertext)

    def loadPublicKey(self, publickey):
        """Load serialized public key string."""

        # Try parse the public key info.
        try:
            if type(publickey) == str:
                j = cipherProduct(publickey)
            else:
                j = publickey
            if j.type != 'RSA_Public_Key':
                raise Exception("This is not a public key thus cannot be loaded.")
            pkdata = self._trim_keydata(j.data, False, False)
            self.bits = int(j.bits)
        except Exception,e:
            raise Exception("Failed loading publickey. Bad format. Error: %s" % e)

        # If parsable, Write down and load.
        try:
            membuff = BIO.MemoryBuffer()
            membuff.write(pkdata)
            self.__publicKey = RSA.load_pub_key_bio(membuff)
        except Exception,e:
            raise Exception("Cannot load public key: %s" % e)

        # Delete existing private key to avoid conflicts.
        self.__privateKey = None

        # Return True after load succeeded.
        return True

    def loadPrivateKey(self, privatekey):
        """Load private key in serialized string."""

        # Try parse the private key info.
        try:
            if type(privatekey) == str:
                j = cipherProduct(privatekey)
            else:
                j = privatekey
            if j.type != 'RSA_Private_Key':
                raise Exception("This is not a private key thus cannot be loaded.")
            pkdata = self._trim_keydata(j.data, True, False)
            self.bits = int(j.bits)
        except Exception,e:
            print str(e)
            raise Exception("Failed loading privatekey. Bad format.")

        # If parsable, Write down and load.
        try:
            membuff = BIO.MemoryBuffer()
            membuff.write(pkdata)
            self.__privateKey = RSA.load_key_bio(membuff)
        except Exception,e:
            raise Exception("Cannot load private key. Error: %s" % e)

        # Override existing public key.
        self._derive_pubkey()

        # Return True if succeeded.
        return True

    def getPublicKey(self, raw=False):
        """Get serialized public key string."""

        # Shall be used with a public key.
        if self.__publicKey == None:
            return False

        # Retrive pubkey data
        membuff = BIO.MemoryBuffer()
        self.__publicKey.save_pub_key_bio(membuff)
        pubkeydata = membuff.read_all()

        # Write down a good form of public key.
        pkinfo = cipherProduct()
        pkinfo.type = 'RSA_Public_Key'
        pkinfo.bits = self.bits
        pkinfo.data = self._trim_keydata(pubkeydata, False, True)
        if raw:
            return pkinfo
        return str(pkinfo)

    def getPrivateKey(self, raw=False):
        """Get serialized string of private key."""

        # Shall work with a private key.
        if self.__privateKey == None:
            return False

        # Retrive privatekey data
        membuff = BIO.MemoryBuffer()
        self.__privateKey.save_key_bio(membuff, None)
        prvkeydata = membuff.read_all()

        # Write down a good form of public key.
        pkinfo = cipherProduct()
        pkinfo.type = 'RSA_Private_Key'
        pkinfo.bits = self.bits
        pkinfo.data = self._trim_keydata(prvkeydata, True, True)
        if raw:
            return pkinfo
        return str(pkinfo)

    def _derive_pubkey(self):
        # derive RSA public key instance from self.__privateKey
        if self.__privateKey == None:
            return False
        membuff = BIO.MemoryBuffer()
        self.__privateKey.save_pub_key_bio(membuff)  #(filename)
        self.__publicKey = RSA.load_pub_key_bio(membuff)   #(filename)

    def _trim_keydata(self,data,isPrivate,operation):
        """This function removes PEM packaged key block's header and footer,
           and decode the base64 encoded data, or inverse. This is used to
           reduce data length."""

        if operation: # True: Trim the data
            if isPrivate:
                data = data[31:]   # -----BEGIN RSA PRIVATE KEY-----
                data = data[:-29]  # -----END RSA PRIVATE KEY-----
            else:
                data = data[26:]
                data = data[:-24]
            return data.strip().decode('base64')
        else: # False: Reconstruct the data
            data = data.encode('base64').strip()
            if isPrivate:
                data = "-----BEGIN RSA PRIVATE KEY-----\n%s\n-----END RSA PRIVATE KEY-----" % data
            else:
                data = "-----BEGIN PUBLIC KEY-----\n%s\n-----END PUBLIC KEY-----" % data
            return data
