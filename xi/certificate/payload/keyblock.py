# -*- coding: utf-8 -*-

from . import payload
from ..object import cipherProduct

class keyblockPayload(payload):
    """Payload that carries a key block."""
    _parameters = {}

    def __init__(self, serializedString = None):
        """Initialize the instance.

           You can optionally specifying `serializedString` to initialize
           an instance from serialized data.
        """
        if type(serialzedString) == str:
            try:
                serializer = cipherProduct()
                self._parameters = {}
                self._parameters['key'] = serializer.k
                self._parameters['base'] = serializer.b
                self._parameters['attach'] = serializer.a
            except:
                self._parameters = {}

    def new(self, carriedKey, baseInfo, attachValues):
        """
           :carriedKey  The public key being signed.
           :BaseInfo    The base info with whom the issuer believed owning
                        this key.
           :attachValues
                        Some tiny strings for authorizing purposes. Can only
                        be an array of strings each no longer than 32 chars
                        in length, with the array containing no more than
                        32 strings.
                            Such restrictions are designed to avoid attempted
                        collisions. Trying to load a serialized string
                        violating such restrictions will lead verification
                        always fail.
        """
        self._parameters = {}

        try:
            if not keyBlock.__class__.__name__ == 'keyBlock':
                return False
            else:
                self._parameters['key'] = keyBlock.id()

            if not baseInfo.__class__.__name__ == 'baseInfo':
                return False
            else:
                self._parameters['base'] = baseInfo.id()

            if type(attachValues) != list:
                return False
            else:
                self._parameters['attach'] = attachValues
        except:
            return False
        
        if not self._validate():
            return False

    def _validate(self):
        for each in self._parameters['attach'] \
                    + [self._parameters['key'], self._parameters['base']]:
            if type(each) != str:
                return False

        if len(self._parameters['key']) != 32:
            return False

        if len(self._parameters['base']) != 32:
            return False

        for each in self._parameters['attach']:
            if len(each) > 32:
                return False

        return True

    def __str__(self):
        if not self._validate():
            return ''

        serializer = cipherProduct()
        serializer.k = self._parameters['key']
        serializer.b = self._parameters['base']
        serializer.a = self._parameters['attach']

        return str(serializer)
