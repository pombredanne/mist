# -*- coding: utf-8 -*-

__ALL__ = ['keyblock', 'plaintext']

class payload:
    """Generic `payload` class.

       Inherit this class and do your own."""
    pass
