# -*- coding: utf-8 -*-

from carrot.connection import BrokerConnection
from carrot.messaging import Consumer as carrotConsumer
from carrot.messaging import Publisher as carrotPublisher

_KEY_PREFIX = 'NERV-mist-rabbitMQ'

def _getconn(host, user, pw, port, vhost):    
    conn = BrokerConnection(hostname=host,
                            port=port,
                            userid=user,
                            password=pw,
                            virtualhost=vhost)
    return conn

class consumer:
    def __init__(self,
                 host,
                 user,
                 pw,
                 port=5672,
                 vhost='/mist_rabbitmq'):
        self._conn = _getconn(host, user, pw, port, vhost)
        self._consumers = {}

    def register(self, func, channel, key, **argv):
        cid = len(self._consumers.keys())
        self._consumers[cid] = [carrotConsumer(connection=self._conn,
                                               queue='NERV-mist',
                                               exchange="%s-%s" % (_KEY_PREFIX, channel),
                                               routing_key=key,
                                               **argv),
                                func]
        return cid

    def poll(self):
        for each in self._consumers:
            message = self._consumers[each][0].fetch()
            if message:
                self._consumers[each][1](payload=message.payload,
                                         key=message.delivery_info['routing_key'])
                message.ack()

    def remove(self, cid):
        self._consumers[cid][0].close()
        del self._consumers[cid]

    def close(self):
        for each in self._consumers.keys():
            self.remove(each)

class publisher:
    def __init__(self,
                 host,
                 user,
                 pw,
                 port=5672,
                 vhost='/mist_rabbitmq'):
        self._conn = _getconn(host, user, pw, port, vhost)

    def publish(self, data, channel, key, **argv):
        publisher = carrotPublisher(connection=self._conn,
                                    exchange="%s-%s" % (_KEY_PREFIX, channel),
                                    routing_key=key,
                                    **argv)
        publisher.send(data)
        publisher.close()
